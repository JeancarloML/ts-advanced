console.log(Math.PI);

class MyMath {
  static readonly PI: number = 3.14;

  static max(...numbers: number[]) {
    return numbers.reduce((max, i) => (i > max ? i : max), numbers[0]);
  }
}

console.log(MyMath.PI);
console.log(MyMath.max(1, 2, 4, 5, 6, 7, 8, 9, 10));
