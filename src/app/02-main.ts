import { ProductMemoryService } from './services/product-memory.service';

const productService = new ProductMemoryService();

productService.create({
  title: 'Product 1',
  price: 100,
  categoryId: 1,
  description: 'Product 1',
  images: ['https://picsum.photos/200/300/?random'],
});

const products = productService.getAll();
const productId = products[0].id;

productService.update(productId, {
  title: 'Product 2',
  price: 200,
  categoryId: 2,
  description: 'Product 2',
  images: ['https://picsum.photos/200/300/?random'],
});

console.log(productService.findOne(productId));
