import { Product } from './product.model';
import { UpdateProductDto, CreateProductDto } from '../dtos/product.dto';
/* export abstract class ProductService {
  abstract getAll(): Promise<Product[]> | Product[];
  abstract create(data: CreateProductDto): Promise<Product> | Product;
  abstract updateProduct(id: Product['id'], changes: UpdateProductDto): Promise<Product> | Product;
  abstract findOne(id: Product['id']): Promise<Product | undefined> | Product | undefined;
} */

export interface ProductService {
  getAll(): Promise<Product[]> | Product[];
  create(data: CreateProductDto): Promise<Product> | Product;
  update(id: Product['id'], changes: UpdateProductDto): Promise<Product> | Product;
  findOne(id: Product['id']): Promise<Product | undefined> | Product | undefined;
}
