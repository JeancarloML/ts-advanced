import { CreateProductDto, UpdateProductDto } from '../dtos/product.dto';
import { Product } from '../models/product.model';
import { ProductService } from '../models/product-service.model';
import axios from 'axios';

export class ProductHttpService implements ProductService {
  private url = 'http://api.escuelajs.co/api/v1/products';
  async getAll() {
    const { data } = await axios.get<Product[]>(this.url);
    return data;
  }
  async create(dto: CreateProductDto) {
    const { data } = await axios.post<Product>(this.url, dto);
    return data;
  }
  async update(id: number, changes: UpdateProductDto) {
    const { data } = await axios.put<Product>(`${this.url}/${id}`, changes);
    return data;
  }
  async findOne(id: number) {
    const { data } = await axios.get<Product>(`${this.url}/${id}`);
    return data;
  }
}
/*
// Singleton
export class ProductHttpService implements ProductService {
  private url = 'http://api.escuelajs.co/api/v1/products';
  private static instance: ProductHttpService | null = null;
  private _httpCount = 0;
  private constructor() {}

  get httpCount() {
    return this._httpCount;
  }

  async getAll() {
    const { data } = await axios.get<Product[]>(this.url);
    this._httpCount++;
    return data;
  }
  async create(dto: CreateProductDto) {
    const { data } = await axios.post<Product>(this.url, dto);
    return data;
  }
  async update(id: number, changes: UpdateProductDto) {
    const { data } = await axios.put<Product>(`${this.url}/${id}`, changes);
    return data;
  }
  async findOne(id: number) {
    const { data } = await axios.get<Product>(`${this.url}/${id}`);
    return data;
  }

  static getInstance() {
    if (!ProductHttpService.instance) {
      ProductHttpService.instance = new ProductHttpService();
      console.log('Creando instancia');
    }
    return ProductHttpService.instance;
  }
}

class AppComponent {
  constructor(private productService: ProductHttpService) {}

  showCount() {
    this.productService.getAll().then((products) => {
      // console.log(products);
      console.log(this.productService.httpCount);
    });
  }
}

const app = new AppComponent(ProductHttpService.getInstance());
app.showCount();

const app2 = new AppComponent(ProductHttpService.getInstance());
app2.showCount(); */
