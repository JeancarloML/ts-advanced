import { Category, AccessType } from '../models/category.model';
import { IsNotEmpty, IsUrl, Length, IsEnum, validateOrReject, validate } from 'class-validator';

export interface ICreateCategoryDto extends Omit<Category, 'id'> {}

export class CreateCategoryDto implements ICreateCategoryDto {
  @IsNotEmpty()
  @Length(3, 50)
  name!: string;
  @IsUrl()
  @IsNotEmpty()
  image!: string;

  @IsNotEmpty()
  @IsEnum(AccessType)
  access?: AccessType;
}

(async () => {
  try {
    const dto = new CreateCategoryDto();
    dto.name = 'aqwqwqw';
    dto.image = 'https://api.escuelajs.co/api/v1/products';
    dto.access = AccessType.PUBLIC;
    await validateOrReject(dto);
  } catch (error) {
    console.log(error);
  }
})();
