export class Animal {
  // Propiedades Protegidas solo pueden ser accedidas desde la clase o clases que hereden de esta clase
  constructor(protected name: string) {}

  move() {
    console.log('Moving along!');
  }

  greeting() {
    return `Hello, I'm ${this.name}`;
  }

  protected doSomething() {
    console.log('dooo');
  }
}

export class Dog extends Animal {
  // No es necesario colocar public en la propiedad ya que pertenece a la clase padre
  constructor(name: string, public owner: string) {
    super(name);
  }

  woof(times: number): void {
    for (let index = 0; index < times; index++) {
      console.log(`woof! ${this.name}`);
    }
    this.doSomething();
  }

  // Sobreescritura de metodos de la clase padre
  move() {
    // code
    console.log('moving as a dog');
    super.move();
  }
}

const cheis = new Dog('cheis', 'nico');
// cheis.name = 'otro nombre';
cheis.woof(1);
// cheis.doSomething();
cheis.move();
